﻿/*
 *  Implementation of Magic Star with 4 sides
 * 
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicStar
{
    class MagicStar4 : MagicStarBase
    {
        public MagicStar4(int nSides)
        {
            base.numSides = nSides;
        }

        /*
         * 
         *        In this 4-sided star, we have A..H as unique numbers
         *  
         *                      A 
         *
         *                  H       B 
         *
         *              G               E
         *
         *                  F       C
         *          
         *                      D 
         *
         *  such that,            
         *    A+B+C+D   = 18
         *    E+C+F+G   = 18
         *    D+F+H+A   = 18
         *    G+H+B+E   = 18
         * 
         *      Node values for numSides = 4
         *              A   B   C   D   E   F   G   H 
         * nodeIndex    0   1   2   3   4   5   6   7
         * assignIndex  0   1   2       3   4        
         * calcIndex                0           1   2
         *
         */

        override public int[] GetNodeValuesFromGeneratedValues(int[] generatedValues, int maxGen)
        {

            // ToDo: generalize for the non-4-sided star
            // ToDo: optimize the computation here to reduce costs
            int[] nodeValues = new int[NumNodes];

            nodeValues[0] = generatedValues[0];
            nodeValues[1] = generatedValues[1];

            nodeValues[2] = generatedValues[2];
            nodeValues[4] = generatedValues[3];

            nodeValues[5] = generatedValues[4];

            nodeValues[3] = FindMissingValueOnSide(nodeValues, 0, 1, 2);    // D = sideSum-A-B-C;
            nodeValues[6] = FindMissingValueOnSide(nodeValues, 2, 4, 5);    // G = sideSum-C-E-F;

            nodeValues[7] = FindMissingValueOnSide(nodeValues, 0, 3, 5);    // H = sideSum-A-D-F;

            // validate that all the nodes have unique and non-zero numbers in them
            return (nodeValues);
        }
    } // class MagicStar4
}