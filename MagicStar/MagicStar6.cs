﻿/*
 *  Implementaion for MagicStar of size 6 sides
 * 
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicStar
{
    class MagicStar6 : MagicStarBase
    {
        public MagicStar6(int nSides)
        {
            base.numSides = nSides;
        }

        /*
         *  
         *        In this 6-sided star, we have A..L as unique numbers
         *  
         *                      A 
         *
         *          L       K       B       E
         *
         *              J               C
         *
         *          I       H       F       D
         *          
         *                      G 
         *
         *  such that,            
         *    A+B+C+D   = 26
         *    E+C+F+G   = 26
         *    D+F+H+I   = 26
         *    G+H+J+K   = 26
         *    I+J+K+A   = 26
         *    L+K+B+E   = 26
         * 

         *      Node values for numSides = 6
         *              A   B   C   D   E   F   G   H   I   J   K   L
         * nodeIndex    0   1   2   3   4   5   6   7   8   9   10  11
         * assignIndex  0   1   2       3   4       5       6   
         * calcIndex                0           1       2       3   4
         * 
         */

        override public int[] GetNodeValuesFromGeneratedValues(int[] generatedValues, int maxGen)
        {
            // for now assume this is a 6-sided star
            // ToDo: generalize for the non-6-sided star
            // ToDo: optimize the computation here to reduce costs
            int[] nodeValues = new int[NumNodes];

            nodeValues[0] = generatedValues[0];
            nodeValues[1] = generatedValues[1];

            nodeValues[2] = generatedValues[2];
            nodeValues[4] = generatedValues[3];

            nodeValues[5] = generatedValues[4];
            nodeValues[7] = generatedValues[5];

            nodeValues[9] = generatedValues[6];

            nodeValues[3] = FindMissingValueOnSide(nodeValues, 0, 1, 2);    // D = sideSum-A-B-C;
            nodeValues[6] = FindMissingValueOnSide(nodeValues, 2, 4, 5);    // G = sideSum-C-E-F;

            nodeValues[8] = FindMissingValueOnSide(nodeValues, 3, 5, 7);    // I = sideSum-D-F-H;

            nodeValues[10] = FindMissingValueOnSide(nodeValues, 0, 8, 9);   // K = sideSum-A-I-J;
            nodeValues[11] = FindMissingValueOnSide(nodeValues, 1, 4, 10);  // L = sideSum-B-E-K

            // validate that all the nodes have unique and non-zero numbers in them
            return (nodeValues);
        }
    } // class MagicStar6
}