﻿/*
 *  Interface contract for n-sided Magic Star
 * 
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicStar
{
    class MagicStarBase : IMagicStar
    {
        const int MAX_GEN_LIMIT = 50000000; // 50M
        public static IMagicStar GetMagicStarInstance(int numSides)
        {
            IMagicStar ims = null;

            switch (numSides)
            {
                case 4:
                    ims = new MagicStar4(numSides);
                    break;

                case 5:
                    ims = new MagicStar5(numSides);
                    break;

                case 6:
                    ims = new MagicStar6(numSides);
                    break;

                case 8:
                    ims = new MagicStar8(numSides);
                    break;

                default:
                    throw new ArgumentException("No valid implementation of Magic star for given Size", "numSides");
            } // switch

            return ims;
        } // GetMagicStarInstance

        internal int numSides = 0;

        public int NumSides { get { return this.numSides; } }

        public int NumNodes { get { return this.numSides * 2; } }

        public int SumForStar { get { return (this.numSides * 4 + 2); } }

        public static void PrintNodeValues(int[] nodeValues, int numNodes)
        {
            Console.Write(": ");
            for (int i = 0; i < numNodes; i++)
            {
                Console.Write(" {0,2:D}", nodeValues[i]);
            }
            Console.WriteLine();
        }

        public static void PrintNodeValues(string message, int[] nodeValues, int numNodes)
        {
            Console.Write("{0,5}", message);
            PrintNodeValues(nodeValues, numNodes);
        }


        internal int FindMissingValueOnSide(int[] nodeValues, int n1, int n2, int n3)
        {
            int x = SumForStar - nodeValues[n1] - nodeValues[n2] - nodeValues[n3];
            return (((x > 0) && (x <= NumNodes)) ? x : 0);
        }

        private bool ValidateNodeValues(int[] nodeValues)
        {
            if (null == nodeValues) return false;

            bool[] isUsed = new bool[NumNodes];
            for (int i = 0; i < NumNodes; i++)
            {
                isUsed[i] = false;
            }

            for (int i = 0; i < NumNodes; i++)
            {
                if (nodeValues[i] == 0)
                {
//                    Console.WriteLine("*************Empty Node value found at i={0}", i);
                    return false;   // a value is absent
                }
                if (isUsed[nodeValues[i] - 1]) return false;   // this value is already present!
                isUsed[nodeValues[i] - 1] = true; // mark that this value is now present
            }

            return true;
        }

        private int[] AssignInitialValues()
        {
            int[] assignedValues = new int[NumSides + 1];
            // 1. initialize the assigned values
            for (int l = 0; l < NumSides + 1; l++)
            {
                assignedValues[l] = l + 1; // make initial assignments sequentially
            }

            return assignedValues;
        }

        virtual public int[] GetNodeValuesFromGeneratedValues(int[] generatedValues, int maxGen)
        {
            return this.GetNodeValuesFromGeneratedValues(generatedValues, maxGen);
        }

        public List<int[]> GenerateNodeValues()
        {
            int solutionIndex = 0;
            int iterIndex = 0;
            List<int[]> allNodeValues = new List<int[]>();

            // 1. initialize the assigned values
            int[] assignedValues = AssignInitialValues();

            // repeat in a loop to generate all solutions
            do
            {
                if ((++iterIndex % 1000000 == 0))
                {
                    Console.WriteLine("-------------------------------------- Iteration {0} ... ", iterIndex);
                }

                if ((iterIndex % MAX_GEN_LIMIT)==0)
                {
                    Console.WriteLine("\n\n--REACHED MAX LIMIT OF ITERATIONS. STOPPING LOOP NOW AT Iteration {0} ... ", iterIndex);
                    break;
                }

                // 2. generate node values using assigned values
                int[] newNodeValues = GetNodeValuesFromGeneratedValues(assignedValues, NumSides + 1);

                // 3. if valid value then we have a new solution; add solution to list
                if ( ValidateNodeValues(newNodeValues))
                {
                    Console.WriteLine("Found a solution. index= {0}", solutionIndex);
                    allNodeValues.Add(newNodeValues);
                    solutionIndex++;
                }

                // 4. increment node sequence for finding the next solution
                //  propagate the increments up the chain if we reach the end of nodes at level
                assignedValues[numSides]++;
                for (int pendingAssign = numSides;
                        (pendingAssign > 0) && (assignedValues[pendingAssign] > NumNodes);
                        pendingAssign--)
                {
                    assignedValues[pendingAssign] = 1; // reset to start of the values
                    assignedValues[pendingAssign - 1]++;   // reset the next node to higher value
                }

            } while (assignedValues[0] <= NumNodes);

            Console.WriteLine("Total of {0} solutions found in {1} iterations", solutionIndex, iterIndex);

            return allNodeValues;

        } // GenerateNodeValues()
    } // class MagicStarBase
}
