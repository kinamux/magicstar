﻿/*
 *  Interface contract for n-sided Magic Star
 * 
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicStar
{
    /// <summary>
    /// IMagicStar is the interface for all MagicStar implementations
    /// </summary>
    interface IMagicStar
    {

        int NumSides { get; }

        int NumNodes { get; }

        List<int[]> GenerateNodeValues();
    }
}
