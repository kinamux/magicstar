﻿/*
 *  The code in this file generates combination of numbers used to fill in 6-gon star    
 * 
 *  Draft1 - Generate all possible solutions using enumeration with minimal optimizations
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 * 
 *  Pending
 *      Generalize for any sided star pattern
 *      Classify solutions to group them by rotation and mirror symmetry
 *      Find unique solutions 
 *      Generate a random mix with some itmes dropped to let the user solve partial star
 */
 
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicStar
{
    /*
     *  A Magic star is a shape that consists of n-sides such that
     *  we can fill in numbers 1...2n each number appearing once and 
     *      sum of all numbers on a single line add up to 4n+2
     * 
     * 
     */

    class MagicStar1
    {
        static void Main(string[] args)
        {
            int numSides = 6;   // start with 6 sides for now

            // Use the input arguments to size the # sides
            bool fInvalidNumSides = true;
            do
            {
                Console.Write("Enter # of sides for Magic star from any of [4, 5, 6, 8]: ");
                string inputNum = Console.ReadLine();

                if (!Int32.TryParse(inputNum, out numSides))
                {
                    Console.WriteLine("Invalid input provided for number of sides ({0}). Try again.", inputNum);
                    continue;
                }

                if ((numSides >= 4) && (numSides <= 8))
                {
                    fInvalidNumSides = false;
                    break;
                }

            } while (fInvalidNumSides);

            Console.WriteLine("Generate the magic star numbers for # sides = {0, 4:D2}", numSides);

            IMagicStar ims = MagicStarBase.GetMagicStarInstance(numSides);

            List<int[]> allSolutions = ims.GenerateNodeValues();

            Console.WriteLine("Total of {0, 4:D4} solutions found", allSolutions.Count);
            for(int i = 0; i < allSolutions.Count; i++)
            {
                MagicStarBase.PrintNodeValues( i.ToString(), allSolutions[i], ims.NumNodes);
            }
        }
    } // Main()
} // namespace
