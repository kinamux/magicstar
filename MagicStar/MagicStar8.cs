﻿/*
 *  Implementaion for MagicStar of size 6 sides
 * 
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicStar
{
    class MagicStar8 : MagicStarBase
    {
        public MagicStar8(int nSides)
        {
            base.numSides = nSides;
        }

        /*
         *  
         *        In this 8-sided star, we have A..P as unique numbers
         *  
         *                A 
         *           O  P  B  E
         *           N        C
         *         M            D
         *           L        F
         *           K  J  H  G
         *                I
         *  such that,            
         *    A+B+C+D   = 34        E+C+F+G = 34
         *    D+F+H+I   = 36        G+H+J+K = 34
         *    I+J+L+M   = 34        K+L+N+O= 34    
         *    M+N+P+A   = 34        O+P+B+E = 34
         * 
         *      Node values for numSides = 8
         *              A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P
         * nodeIndex    0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
         * assignIndex  0   1   2       3   4       5       6       7       8
         * calcIndex                0           1       2       3       4       5   6
         * 
         */

        override public int[] GetNodeValuesFromGeneratedValues(int[] generatedValues, int maxGen)
        {
            // for now assume this is a 6-sided star
            // ToDo: generalize for the non-6-sided star
            // ToDo: optimize the computation here to reduce costs
            int[] nodeValues = new int[NumNodes];

            nodeValues[0] = generatedValues[0];
            nodeValues[1] = generatedValues[1];

            nodeValues[2] = generatedValues[2];
            nodeValues[4] = generatedValues[3];

            nodeValues[5] = generatedValues[4];
            nodeValues[7] = generatedValues[5];

            nodeValues[9] = generatedValues[6];
            nodeValues[11] = generatedValues[7];

            nodeValues[13] = generatedValues[8];

            nodeValues[3] = FindMissingValueOnSide(nodeValues, 0, 1, 2);    // D = sideSum-A-B-C;
            nodeValues[6] = FindMissingValueOnSide(nodeValues, 2, 4, 5);    // G = sideSum-C-E-F;

            nodeValues[8] = FindMissingValueOnSide(nodeValues, 3, 5, 7);    // I = sideSum-D-F-H;
            nodeValues[10] = FindMissingValueOnSide(nodeValues, 6, 7, 9);   // K = sideSum-G-H-J;

            nodeValues[12] = FindMissingValueOnSide(nodeValues, 8, 9, 11);  // M = sideSum-I-J-L
            nodeValues[14] = FindMissingValueOnSide(nodeValues, 10, 11, 13);  // O = sideSum-K-L-N

            nodeValues[15] = FindMissingValueOnSide(nodeValues, 0, 12, 13);  // P = sideSum-M-N-A

            // validate that all the nodes have unique and non-zero numbers in them
            return (nodeValues);
        }
    } // class MagicStar8
}