﻿/*
 *  Implementation of Magic Star with 5 sides
 * 
 *
 *  Kinamux, May 2014
 *  kinamux@outlook.com
 *
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace MagicStar
{
    class MagicStar5 : MagicStarBase
    {
        public MagicStar5(int nSides)
        {
            base.numSides = nSides;
        }

        /*
         * 
         *        In this 5-sided star, we have A..J as unique numbers
         *  
         *                          A 
         *                I     J       B      E
         *                   H             C
         *                          F
         *                G                    D
         *                      
         *  such that,            
         *    A+B+C+D   = 22
         *    E+C+F+G   = 22
         *    D+F+H+I   = 22
         *    G+H+J+A   = 22
         *    I+J+B+E   = 22
         * 
         *      Node values for numSides = 5
         *              A   B   C   D   E   F   G   H   I   J
         * nodeIndex    0   1   2   3   4   5   6   7   8   9
         * assignIndex  0   1   2       3   4       5
         * calcIndex                0           1       2   3
         *
         */

        override public int[] GetNodeValuesFromGeneratedValues(int[] generatedValues, int maxGen)
        {
            // ToDo: generalize for the non-5-sided star
            int[] nodeValues = new int[NumNodes];

            nodeValues[0] = generatedValues[0];
            nodeValues[1] = generatedValues[1];

            nodeValues[2] = generatedValues[2];
            nodeValues[4] = generatedValues[3];

            nodeValues[5] = generatedValues[4];
            nodeValues[7] = generatedValues[5];

            nodeValues[3] = FindMissingValueOnSide(nodeValues, 0, 1, 2);    // D = sideSum-A-B-C;
            nodeValues[6] = FindMissingValueOnSide(nodeValues, 2, 4, 5);    // G = sideSum-C-E-F;

            nodeValues[8] = FindMissingValueOnSide(nodeValues, 3, 5, 7);    // I = sideSum-D-F-H;
            nodeValues[9] = FindMissingValueOnSide(nodeValues, 0, 6, 7);    // J = sideSum-A-G-H;

            // validate that all the nodes have unique and non-zero numbers in them
            return (nodeValues);
        }
    } // class MagicStar5
}